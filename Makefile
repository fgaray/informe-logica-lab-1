all:
	pdflatex prueba.tex
	biber prueba
	pdflatex prueba.tex
	biber prueba
	pdflatex prueba.tex

clean:
	rm prueba.lof
	rm prueba.lot
	rm prueba.aux
	rm prueba.bbl
	rm prueba.toc
	rm prueba.log
	rm prueba.blg
